import { useState } from 'react'

const AddProduct = ({ onAdd }) => {
    const [image, setImage] = useState('')
    const [name, setName] = useState('')
    const [price, setPrice] = useState('')

    const onSubmit = (e) => {
        e.preventDefault()

        if(!image, !name, !price ) {
            alert('Please add a product')
            return
        }

        onAdd({ image, name, price })
        setImage('')
        setName('')
        setPrice('')
    }

    return (
        <form className='add-form' onSubmit={onSubmit}>
           <div className='form'>
             <label></label>
             <input type='text' placeholder='Product image'
                value={image}
                onChange={(e) => setImage(e.target.value)}/>
            </div> 
            <div className='form'>
             <label></label>
             <input type='text' placeholder='Product name'
                value={name}
                onChange={(e) => setName(e.target.value)}/>
            </div>
            <div className='form'>
             <label></label>
             <input type='text' placeholder='Product price'
                value={price}
                onChange={(e) => setPrice(e.target.value)}/>
            </div>

            <input type='submit' value='Submit' className='btn'/>
        </form>
        
    )
}

export default AddProduct