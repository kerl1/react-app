import { FaTimes } from 'react-icons/fa'

const Product = ({product, onDelete}) => {
    return (
        <div className= 'product'>
            <FaTimes style={{color: 'red', float: 'right'}} onClick={() => onDelete(product.id)}
            />
            <br></br>
            <p>{product.image}</p>
            <p>{product.name}</p>
            <p>{product.price}</p>
        </div>
    )
}

export default Product