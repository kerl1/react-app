import { useState } from 'react'
import './App.css';
import Header from './components/header/Header';
import Products from './components/products/Products'
import AddProduct from './components/addProduct/AddProduct'

const App = () => {
const [products, setProducts] = useState ([
  {
      id: 1,
      image: 'first image',
      //image: require('./images/Product_Type.jpg'), 
      name: 'first product',
      price: 500,
  },
  {
      id: 2,
      image: 'second image',
      //image: require('./images/Product_Type.jpg'),
      name: 'second product',
      price: 300,
  },

])

//Add product
const addProduct = (product) => {
  const id = Math.floor(Math.random() * 10000) + 1
  const newProduct = { id, ...product }
  setProducts([...products, newProduct])
}

//Delete product
const deleteProduct = (id) => {
  setProducts(products.filter((product) => product.id !== id))
}

  return (
    console.log(products),
    <div className="App">
      <Header/>
      <AddProduct onAdd={addProduct}/>
      {products.length > 0 ? (
      <Products products={products} onDelete={deleteProduct} />
      ) : (
        'No Products To Show'
      )}
    </div>
  );
}

export default App;
